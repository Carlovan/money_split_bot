use teloxide::types::Message;
use crate::config::Config;

pub fn is_from_admin(msg: Message, conf: Config) -> bool {
    msg.from().map(|user| user.id == conf.admin).unwrap_or(false)
}

pub fn is_from_allowlisted_user() -> bool { true }
