use teloxide::types::UserId;

#[derive(Clone)]
pub struct Config {
    pub admin: UserId,
}

pub fn get_default() -> Config {
    Config {
        admin: UserId(62805296)
    }
}
