use teloxide::prelude::*;

use split_bot::config;
use split_bot::authz::{is_from_admin, is_from_allowlisted_user};

#[tokio::main]
async fn main() {
    pretty_env_logger::init();
    log::info!("Starting throw dice bot...");

    let bot = Bot::from_env().auto_send();

    let handler = Update::filter_message()
        .filter(is_from_allowlisted_user)
        .branch(
            dptree::filter(is_from_admin)
            .inspect_async(|msg: Message, bot: AutoSend<Bot>| async move {
                bot.send_message(msg.chat.id, format!("Good morning my lord {}.", get_name(&msg)))
                    .reply_to_message_id(msg.id)
                    .await.log_on_error()
                    .await;
            })
        )
        .endpoint(|msg: Message, bot: AutoSend<Bot>| async move {
            bot.send_message(msg.chat.id, format!("Hello {}", get_name(&msg)))
                .reply_to_message_id(msg.id)
                .await?;
            respond(())
        });

    Dispatcher::builder(bot, handler)
        .dependencies(dptree::deps![config::get_default()])
        .enable_ctrlc_handler()
        .build()
        .dispatch()
        .await;
}

fn get_name(msg: &Message) -> &str {
    msg.from().map(|usr| usr.first_name.as_str()).unwrap_or("")
}
