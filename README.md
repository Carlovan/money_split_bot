# MoneySplitBot

## Introduction

This bot was born to easily track shared money with my girlfriend.
It will not have any feature for multi-user support, but I will keep it flexible enough to add such features in the future.

I will add some comments in the code with the format `TODO@multiuser` or in general `TODO@<featureName>` to track places that need attention to implement that feature.

## Use cases

### Add a transaction
Basic functionality, add a transaction in the context given the amount paid. Since there are only two people, the receiver is implicit.

**Requirements**:
- Payer can be self (implicit) or someone else.
- [Optional] Asks confirmation.
- [Optional] Choose if the amount is shared (50/50) or payed entirely for the receiver.

### Create new split context


### Set default context
### Close context
### List contexts
### Show transactions
### Show balances
